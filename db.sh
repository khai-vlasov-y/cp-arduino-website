#!/bin/bash

CNAME='cp-postgres'

# docker stop -t 0 $CNAME;
# docker rm $CNAME;
if ! docker inspect $CNAME &>/dev/null; then
	docker build -t $CNAME -f Dockerfile-db . &&
	docker run -d --restart always \
	    --name $CNAME \
	    -p 5432:5432 \
	    -e POSTGRES_USER=$POSTGRES_USER \
	    -e POSTGRES_PASSWORD=$POSTGRES_PASSWORD \
	    --net nginx-proxy $CNAME
fi