-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2018-04-23 12:23:50.845

-- tables
-- Table: arduino
CREATE TABLE arduino (
    id serial  NOT NULL,
    guid varchar(36)  NOT NULL,
    cryptokey varchar(32)  NOT NULL,
    description varchar(1024)  NULL,
    last_sent timestamp  NULL,
    fk_format_script int  NOT NULL,
    CONSTRAINT arduino_pk PRIMARY KEY (id)
);

CREATE INDEX arduino_idx_guid on arduino (guid ASC);

-- Table: data
CREATE TABLE data (
    id serial  NOT NULL,
    fk_arduino_id int  NOT NULL,
    raw varchar(1024)  NOT NULL,
    formatted varchar(2048)  NOT NULL,
    recieved_at timestamp  NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT data_pk PRIMARY KEY (id)
);

-- Table: format_scritps
CREATE TABLE format_scritps (
    id serial  NOT NULL,
    code varchar(4096)  NOT NULL,
    description varchar(1024)  NOT NULL,
    CONSTRAINT format_scritps_pk PRIMARY KEY (id)
);

-- views
-- View: v_arduino_online
CREATE VIEW v_arduino_online AS
SELECT a.id, a.guid, a.description, a.last_sent
  FROM arduino AS a
  WHERE current_timestamp - a.last_sent < INTERVAL '5 minutes';

-- foreign keys
-- Reference: arduino_eval_scritps (table: arduino)
ALTER TABLE arduino ADD CONSTRAINT arduino_eval_scritps
    FOREIGN KEY (fk_format_script)
    REFERENCES format_scritps (id)
    ON DELETE  CASCADE 
    ON UPDATE  CASCADE 
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: data_arduino (table: data)
ALTER TABLE data ADD CONSTRAINT data_arduino
    FOREIGN KEY (fk_arduino_id)
    REFERENCES arduino (id)
    ON DELETE  CASCADE 
    ON UPDATE  CASCADE 
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

CREATE OR REPLACE FUNCTION set_last_sent() RETURNS TRIGGER AS $payment$
  BEGIN
    UPDATE arduino
      SET last_sent = NEW.recieved_at
    WHERE id = NEW.fk_arduino_id;
    RETURN NEW;
END;
$payment$ LANGUAGE plpgsql;
-- Blocks payments with value = 0
CREATE TRIGGER tr_set_last_sent
AFTER INSERT ON data
  FOR EACH ROW WHEN (pg_trigger_depth() = 0)
  EXECUTE PROCEDURE set_last_sent();;
  

INSERT INTO format_scritps (code, description) VALUES
  ('import json
min = 0
max = 40
is_correct = True
try:
    t = float(msg)
    if t < min or t > max:
        is_correct = False
except:
    is_correct = False
result = json.dumps(dict(is_correct=is_correct, data=msg, type="Temperature", min=min, max=max))',
  'Temperature from 0 to 40');

INSERT INTO arduino (guid, cryptokey, description, fk_format_script)
  SELECT
    'ea4f29b2-1971-46ad-be0a-f977b7352517',
    '0123456789abcdef0123456789abcdef',
    'Default temperature sensor',
    s.id
  FROM format_scritps as s
  WHERE description = 'Temperature from 0 to 40';;

-- End of file.

