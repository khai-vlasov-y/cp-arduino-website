# Start with a basic flask app webpage.
from flask import Flask, render_template
from flask.ext.socketio import SocketIO, emit
from sqlalchemy import create_engine, MetaData, Table
from threading import Thread, Event

from time import sleep

import os


engine = create_engine(os.environ['DATABASE_URL'], convert_unicode=True)
metadata = MetaData(bind=engine)


app = Flask(__name__)
app.config.from_object(os.environ['APP_SETTINGS'])

# turn the flask app into a socketio app
socketio = SocketIO(app)

# Display Data Thread
thread = Thread()
thread_stop_event = Event()


class RandomThread(Thread):

    def __init__(self):
        self.delay = 1
        super(RandomThread, self).__init__()

    def randomNumberGenerator(self):
        """
        Refresh data every 1 second and emit to a socketio instance (broadcast)
        Ideally to be run in a separate thread?
        """
        def get_data(table_name, column_names):
            data_result = []

            data_table = Table(table_name, metadata,
                               autoload=True, autoload_with=engine)
            data_table_obj = data_table.select().execute().fetchall()

            for row in data_table_obj:
                data_result.append(dict((key, value) for key, value in dict(row).iteritems() if key in column_names))

            return data_result

        while not thread_stop_event.isSet():
            sensors_online = get_data('v_arduino_online', ['id', 'description', 'last_sent'])
            sensors_data = get_data('data', ['fk_arduino_id', 'formatted', 'recieved_at'])

            socketio.emit('newnumber', {'sensors': sensors_online, 'sensors_data': sensors_data},
                          namespace='/test')
            sleep(self.delay)

    def run(self):
        self.randomNumberGenerator()


@app.route('/')
def index():
    # only by sending this page first will the client be connected to the
    # socketio instance
    return render_template('index.html')


@socketio.on('connect', namespace='/test')
def test_connect():
    # need visibility of the global thread object
    global thread
    print('Client connected')

    # Start the Display Data thread only if the thread has not been
    # started before.
    if not thread.isAlive():
        print "Starting Thread"
        thread = RandomThread()
        thread.start()


@socketio.on('disconnect', namespace='/test')
def test_disconnect():
    print('Client disconnected')


if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0')
