#!/bin/sh

URL='terenik.vlasov.pro'
CNAME=$URL

docker stop -t 0 $CNAME;
docker rm $CNAME;
docker build -t $CNAME -f Dockerfile-web . && \
docker run -d --restart always \
    --name $CNAME \
    -e VIRTUAL_HOST=$URL \
    -e VIRTUAL_PORT=5000 \
    -e APP_SETTINGS="config.DevelopmentConfig" \
    -e CERT_NAME='' \
    -e DATABASE_URL=$DATABASE_URL \
    --expose 5000 \
    --net nginx-proxy $CNAME
